from lxml import html
import sys
import requests
import spotipy
import spotipy.util as util
import re
import webbrowser
import scrapy


def authenth():
    scope = 'user-library-read'
    username = input("Enter username: ")
    token = util.prompt_for_user_token(username, scope, client_id = '6d6b373ad2944dcfa402d870617157b4',
        client_secret = 'df49a5ecfac742f792692dc757834bac', redirect_uri = 'https://github.com/Deniall/spottit')

    if token:
        sp = spotipy.Spotify(auth=token)
        results = sp.current_user_saved_tracks()
        for item in results['items']:
            track = item['track']
            print(track['name'] + ' - ' + track['artists'][0]['name'])
    else:
        print("Can't get token for", username)
        
def scrape(subreddit):
    page = requests.get('https://www.reddit.com/r/chillmusic/',timeout=15)
    tree = html.fromstring(page.content)
    posts = tree.xpath('//p[@class="title"]/a[@class="title may-blank loggedin outbound  srTagged imgScanned"]/text()') # takes only the posts names
    titles = list() # converts tree to list
    for post in posts:
        titles.append(str(post)) # stringifies for ease of use
    return titles # titles is a list of all post names (tracknames + artists)

def lookup(titles):
    sp = spotipy.Spotify()
    for title in titles: # for every artist + song name...
        trackname = re.sub(r'\[[^)]*\]', '', str(title)) # gets rid of garbage flair text
        results = sp.search(q=trackname) # spotify API 'search'
        print("Current song: "+trackname+"")# prints song currently being searches
        for i, t in enumerate(results['tracks']['items']): #neat list of results
            print(' ', i, t['name']) #prints any results it can find


    

def main():
    print("Welcome to Spottit Alpha")
    authenth()
    subreddit = input("Enter the name of the subreddit you wish to scrape Spotify for:  ")
    print(subreddit)
    titleList = scrape(subreddit)
    print(titleList)
    lookup(titleList)

if __name__ == "__main__":
    main()

    


