[![Stories in Ready](https://badge.waffle.io/Deniall/spottit.png?label=ready&title=Ready)](https://waffle.io/Deniall/spottit)
# reddit-to-spotify
A tool that users can use to keep up to date with their favourite music subreddits via Spotify.

### Current Progress: CLI web scraper tool that takes in a 'subreddit' variable of your choosing (eg. futurebeats) and returns all the song names and their spotify search results, if found on Spotify. Next step will be linking the spotify API and added the results to a playlist, and hosting it on a server and having it run once every 30 mins to look for new posts.
